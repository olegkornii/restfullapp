package com.oleg.project.servises;

import com.oleg.project.userdao.UserDaoImp;
import com.oleg.project.users.User;

import java.util.List;

public class UserService {
    private UserDaoImp userDaoImp = new UserDaoImp();

    public User findUserById(int id) {
        return userDaoImp.findById(id);
    }

    public User saveUser(User user) {
        userDaoImp.save(user);
        return user;
    }

    public User updateUser(User user) {
        userDaoImp.update(user);
        return user;
    }

    public User deleteUser(User user) {
        userDaoImp.delete(user);
        return user;
    }

    public List<User> getAllUsers() {
        return userDaoImp.getAll();
    }
}
