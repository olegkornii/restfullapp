package com.oleg.project.web.controller;


import com.oleg.project.servises.UserService;
import com.oleg.project.users.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class MainController {
    UserService userService = new UserService();

    @PostMapping
    public User create(@RequestBody User user) {
        return userService.saveUser(user);
    }

    @GetMapping(path = {"/{id}"})
    public User findOne(@PathVariable("id") int id) {
        return userService.findUserById(id);
    }

    @PutMapping
    public User update(@RequestBody User user) {
        return userService.updateUser(user);
    }

    @DeleteMapping(path = {"/{id}"})
    public User delete(@PathVariable("id") int id) {
        return userService.deleteUser(userService.findUserById(id));
    }

    @GetMapping
    public List findAll() {
        return userService.getAllUsers();
    }
}
