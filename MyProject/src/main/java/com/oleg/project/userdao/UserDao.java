package com.oleg.project.userdao;

import com.oleg.project.users.User;

import java.util.List;

public interface UserDao {
    User findById(int id);

    void save(User user);

    void update(User user);

    void delete(User user);

    List<User> getAll();

}
